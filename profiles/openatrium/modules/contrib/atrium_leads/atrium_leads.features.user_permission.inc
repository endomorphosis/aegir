<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function atrium_leads_user_default_permissions() {
  $permissions = array();

  // Exported permission: create lead content
  $permissions['create lead content'] = array(
    'name' => 'create lead content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: delete any lead content
  $permissions['delete any lead content'] = array(
    'name' => 'delete any lead content',
    'roles' => array(),
  );

  // Exported permission: delete own lead content
  $permissions['delete own lead content'] = array(
    'name' => 'delete own lead content',
    'roles' => array(),
  );

  // Exported permission: edit any lead content
  $permissions['edit any lead content'] = array(
    'name' => 'edit any lead content',
    'roles' => array(
      '0' => 'manager',
    ),
  );

  // Exported permission: edit own lead content
  $permissions['edit own lead content'] = array(
    'name' => 'edit own lead content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  return $permissions;
}
