<?php
/**
 * @file
 * Test definitions for the Storm Person module.
 */
class StormpersonTestCase extends DrupalWebTestCase {

  public static function getInfo() {
    return array(
      'name' => 'Storm Person functionality',
      'description' => 'Test the functionality of the Storm Person module',
      'group' => 'Storm',
    );
  }

  public function setUp() {
    parent::setUp('storm', 'stormorganization', 'stormperson');
    $privileged_user = $this->drupalCreateUser(array('Storm organization: add', 'Storm organization: view all', 'Storm person: add'));
    $this->drupalLogin($privileged_user);
  }

  public function testStormpersonAccess() {
    $this->drupalGet('storm/people');
    $this->assertResponse(403, t('Make sure access is denied to Storm People list for anonymous user'));

    $basic_user = $this->drupalCreateUser();
    $this->drupalLogin($basic_user);
    $this->drupalGet('storm/people');
    $this->assertResponse(403, t('Make sure access is denied to Storm People list for basic user'));

    $privileged_user = $this->drupalCreateUser(array('Storm person: access'));
    $this->drupalLogin($privileged_user);
    $this->drupalGet('storm/people');
    $this->assertText(t('People'), t('Make sure the correct page has been displayed by checking that the title is "People".'));
  }

  public function testStormpersonCreate() {
    $org = array(
      'title' => $this->randomName(32),
      'body' => $this->randomName(64),
    );
    $person = array(
      'title' => $this->randomName(32),
      'body' => $this->randomName(64),
    );

    $this->drupalPost('node/add/stormorganization', $org, t('Save'));
    $this->drupalPost('node/add/stormperson', $person, t('Save'));
    $this->assertText(t('Person @title has been created.', array('@title' => $person['title'])));
  }
}
