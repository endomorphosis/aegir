var ASS = ASS || {};
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    var defaultsId = ASS.defaults.init({}, 'apachesolr-storm-search-form');  
    if ('' == $('#edit-keys-sitewide').val() ) {
      ASS.defaults.set(defaultsId, 'edit-keys-sitewide', Drupal.t('Search Storm'));
    }
  });
}