<?php

/**
 * @file
 * Storm Contrib Log Installation File
 * Implements install, uninstall, enable and requirements hook
 */

/**
 * Implements hook_enabled().
 * Sets default variables every time module will be enabled
 */
function storm_contrib_log_enable() {
  if (module_exists('heartbeat')) {
    variable_set('storm_contrib_log_module', 'heartbeat');
  }
  elseif (module_exists('storm_contrib_logger')) {
    variable_set('storm_contrib_log_module', 'storm_contrib_logger');
  }
  else {
    variable_set('storm_contrib_log_module', false);
    variable_set('storm_contrib_log_active', false);
  }
}

/**
 * Implements hook_requirements().
 * Warns the Administrator if there is no activity module installed.
 *
 * @param $phase
 * @return array
 */
function storm_contrib_log_requirements($phase) {
  $requirements = array();

  if (module_exists('heartbeat')) {
    $requirements['log_module'] = array();
    $requirements['log_module']['title'] = t('Activity/Log Module: Heartbeat');
    $requirements['log_module']['description'] = t('Found Activity as Heartbeat Module.');
    $requirements['log_module']['severity'] = REQUIREMENT_OK;
  }
  elseif (module_exists('storm_contrib_logger')) {
    $requirements['log_module'] = array();
    $requirements['log_module']['title'] = t('Activity/Log Module: Storm Contrib Logger');
    $requirements['log_module']['description'] = t('Found Storm Contrib Logger as Activity Module.');
    $requirements['log_module']['severity'] = REQUIREMENT_OK;
  }
  else {
    $requirements['no_log_module'] = array();
    $requirements['no_log_module']['title'] = t('No Activity/Log Module');
    $requirements['no_log_module']['description'] =
      t('A Activity/Log Module is needed. Please install and enable one of the following modules: !heartbeat or the provided Storm Contrib Logger',
        array(
          '!activity' => l(t('Heartbeat'), 'http://drupal.org/project/heartbeat'),
        )
      );
    $requirements['no_log_module']['severity'] = REQUIREMENT_WARNING;
  }

  return $requirements;
}

/**
 * Implements hook_install().
 * Activates all messages on first initialization.
 */
function storm_contrib_log_install() {
  module_load_include('module', 'storm_contrib_log');
  $messages = storm_contrib_log_heartbeat_message_info();
  $default_messages = array();
  foreach($messages as $key=>$array) {
    $default_messages[$array['message_id']] = $array['message_id'];
  }
  variable_set('storm_contrib_log_enabled_messages', $default_messages);
}

/**
 * Implements hook_uninstall().
 * Removes all variables, that are set by this module.
 */
function storm_contrib_log_uninstall() {
  variable_del('storm_contrib_log_enabled_messages');
  variable_del('storm_contrib_log_module');
  variable_del('storm_contrib_log_active');
  variable_del('storm_contrib_log_cron');
  variable_del('storm_contrib_log_cron_last');
}