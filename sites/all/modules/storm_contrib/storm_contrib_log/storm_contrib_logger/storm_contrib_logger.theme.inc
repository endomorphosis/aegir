<?php


function theme_storm_contrib_logger_message($message) {

  $content = $message['message'];
  if (!empty($message['action'])) {
    $content .= ' <span class="storm-contrib-logger-message-actions">[ '.
                implode(' | ', $message['action']) .
                ' ]</span>';
  }

  return $content;
}

function theme_storm_contib_logger_view_activities_table($messages, $header, $limit) {
  $content = '<div class="storm-contrib-logger-message-table-board">';

  if (count($messages) > 0) {
    $rows = array();
    $icon = "";
    foreach($messages as $message) {
      $title = $message['message_id'];
      $row = array();
      switch($message['message_id']) {
        case 'storm_node_update':
        case 'storm_node_insert':
        case 'stormproject_overdue':
        case 'stormproject_costs_exceeded':
        case 'storm_node_no_assignment':
        case 'storm_node_no_department':
          $title = $message['variables']['@node_type'];
          $icon  = drupal_strtolower($title);
          break;

        case 'stormtimetracking_missing':
          $title = t('Timetracking');
          $icon  = 'report';
          break;

        case 'stormperson_utlisation_exceeded':
          $title = t('Utilisation');
          $icon  = 'report';
          break;
      }
      $icon = storm_icon($icon, $title);
      $row[] = $icon;
      $row[] = format_date($message['timestamp'], 'small');
      $row[] = theme('storm_contrib_logger_message', $message);
      $rows[] = $row;
    }

    $content .= theme('table',$header, $rows );

    if ($limit > 10) {
      $content .= theme('pager', array(), $limit, 0);
    }

  }
  else {
    $content .= "<p>No Activities available here for you.</p>";
  }

  $content .= '</div>';

  return $content;

}