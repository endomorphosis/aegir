<?php

/**
 * @file stormexpense_extension.module
 *
 * extensions and modifications on the stormexpense module
 */


// TODO EXPENSES DO NOT HAVE A CURRENCY YET - ADD IF AVAILABLE

/**
 * Implementation of hook_form_alter()
 *
 * @return unknown_type
 */
function stormexpense_extension_form_alter(&$form, $form_state, $form_id) {

  global $user;

  switch ($form_id) {

    // EXPENSE NODE FORM
    case 'stormexpense_node_form':

      // ----------------------------------------------
      // ADD EMPTY ITEM TO ORGANIZATION AND FORCE USER
      // TO SELECT A PROJECT IF NECESSARY
      // ----------------------------------------------
      storm_contrib_common_organization_add_empty_item($form);

      // ------------------------
      // SET PROJECT AS REQUIRED
      // ------------------------
      if (!empty($form['group1']['project_nid']['#options'])) {
        if (variable_get('stormproject_extension_expense_project_mandantory', FALSE)) {
          $form['group1']['project_nid']['#required'] = true;
        }
      }

      $form['#validate'][] = 'stormexpense_extension_expense_node_form_validate';
      $form['#pre_render'][] = 'stormexpense_extension_pre_render_expense_form';

      break;

  }

}


/**
 * pre render the task form before it is rendered as html
 * update the options of the select forms because they maybe were modified by javascript
 * but the form is fetched from the cache without the modifications
 *
 * @param unknown_type $form
 */
function stormexpense_extension_pre_render_expense_form($form) {

  // --------------------
  // CHECK ORGANIZATIONS
  // --------------------
  if (!empty($form['group1']['organization_nid']['#value']) && $form['group1']['organization_nid']['#value'] != $form['group1']['organization_nid']['#default_value']) {
    $form['group1']['organization_nid']['#default_value'] = $form['group1']['organization_nid']['#value'];
    // OPTIONS ARE THE SAME BECAUSE THEY ARE NOT CHANGED BY JS
  }

  // ----------------
  // CHECK PROJECTS
  // ----------------
  if (!empty($form['group1']['project_nid']['#value']) && ($form['group1']['project_nid']['#value'] != $form['group1']['project_nid']['#default_value'])) {

    $form['group1']['project_nid']['#default_value'] = $form['group1']['project_nid']['#value'];

  }

  // TODO MAYBE SOURCE OUT INTO SEPARATE FUNCTION
  // GET ONLY PROJECTS WITHOUT STATE COMPLETED
  $s = "SELECT n.nid, n.title FROM {stormproject} spr INNER JOIN {node} n ON spr.nid=n.nid WHERE spr.organization_nid=%d AND n.status=1 AND n.type='stormproject' AND NOT spr.projectstatus='completed' ORDER BY n.title";
  $s = stormproject_access_sql($s);
  $s = db_rewrite_sql($s);
  $r = db_query($s, $form['group1']['organization_nid']['#value']);
  $projects = array();
  while ($project = db_fetch_object($r)) {
    $projects[$project->nid] = $project->title;
  }
  $projects = array('' => '-') + $projects;
  $form['group1']['project_nid']['#options'] = $projects;

  return $form;
}


/**
 * validate stormexpense node form
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return unknown_type
 */
function stormexpense_extension_expense_node_form_validate($form, &$form_state) {

  // -------------------
  // CHECK ORGANIZATION
  // -------------------
  if (empty($form_state['values']['organization_nid']) || '' == $form_state['values']['organization_nid']) {
    form_set_error('organization_nid', t('You have to select an organization'));
  }

  // --------------
  // CHECK PROJECT
  // --------------
  if (empty($form_state['values']['project_nid']) || '' == $form_state['values']['project_nid']) {
    if (variable_get('stormproject_extension_expense_project_mandantory', FALSE)) {
      form_set_error('project_nid', t('You have to select a project'));
    }
  }

}


/**
 * Implementation of hook_nodeapi()
 * @return unknown_type
 */
function stormexpense_extension_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

  $types = array('stormproject');

  if (!in_array($node->type, $types)) {
    return;
  }


  switch ($op) {

    // -----
    // LOAD
    // -----
    case 'load':
    case 'prepare':
      // CALCULATE THE TOTAL EXPENSES COSTS
      $node->stormexpenses_total_costs = stormexpense_extension_calculate_total_expense_costs($node, FALSE);
      break;

  }

}


/**
 * calculate the total expenses costs of a type in a certain time period
 *
 * @param $type
 * @param $start_time
 * @param $end_time
 * @return unknown_type
 */
function stormexpense_extension_calculate_total_expense_costs($node_type, $distinct_by_type = FALSE, $start_time = 0, $end_time = 0) {

  $total_costs = 0;

  if (!empty($node_type)) {
    $expenses = stormexpense_extension_get_expenses($node_type, $distinct_by_type, $start_time, $end_time);
    if (!empty($expenses)) {
      foreach ($expenses as $nid_expense => $expense_array) {
        if (!empty($expense_array)) {
          $total_costs += $expense_array['total'];
        }
      }
    }
  }

  return $total_costs;
}


/**
 * get all expenses of a type in a certain time period
 *
 * @param unknown_type $type
 * @param unknown_type $start_time
 * @param unknown_type $end_time
 * @return unknown_type
 */
function stormexpense_extension_get_expenses($node_type, $distinct_by_type = FALSE, $start_time = 0, $end_time = 0) {

  $expenses = array();

  if (!empty($node_type)) {

    $args = array();
    $sql  = 'SELECT sex.*, n.title ';
    $sql .= 'FROM {stormexpense} sex ';
    $sql .= 'LEFT JOIN {node} n USING(nid) ';
    $sql .= 'WHERE n.status = 1 ';

    switch ($node_type->type) {

      case 'stormorganization':
        if (!empty($node_type->nid)) {
          $sql .= 'AND sex.organization_nid = %d ';
          $args[] = $node_type->nid;
        }
        if ($distinct_by_type) {
          $sql .= 'AND (sex.project_nid = \'\' OR sex.project_nid IS NULL) ';
          $sql .= 'AND (sex.task_nid = \'\' OR sex.task_nid IS NULL) ';
          $sql .= 'AND (sex.ticket_nid = \'\' OR sex.ticket_nid IS NULL) ';
        }
        break;

      case 'stormproject':
        if (!empty($node_type->nid)) {
          $sql .= 'AND sex.project_nid = %d ';
          $args[] = $node_type->nid;
        }
        if ($distinct_by_type) {
          $sql .= 'AND (sex.task_nid = \'\' OR sex.task_nid IS NULL) ';
          $sql .= 'AND (sex.ticket_nid = \'\' OR sex.ticket_nid IS NULL) ';
        }
        $project_nid = $node_type;
        break;

      case 'stormtask':
        if (!empty($node_type->nid)) {
          $sql .= 'AND sex.task_nid = %d ';
          $args[] = $node_type->nid;
        }
        if ($distinct_by_type) {
          $sql .= 'AND (sex.ticket_nid = \'\' OR sex.ticket_nid IS NULL) ';
        }
        $project_nid = $node_type->project_nid;
        break;

      case 'stormticket':
        if (!empty($node_type->nid)) {
          $sql .= 'AND sex.ticket_nid = %d ';
          $args[] = $node_type->nid;
        }
        if (!empty($node_type->project_nid)) {
          $project_nid = $node_type->project_nid;
        }
        break;

    }

    if (!empty($start_time)) {
      $sql .= 'AND sex.expensedate >= %d ';
      $args[] = $start_time;
    }

    if (!empty($end_time)) {
      $sql .= 'AND sex.expensedate <= %d ';
      $args[] = $end_time;
    }

    //additional access checks for Storm invoice
    $access_check = TRUE;
    if (!empty($project_nid)) {
      global $user;
      if (is_object($project_nid)) {
        $project_node = $project_nid;
      }
      else {
        $project_node = node_load($project_nid);
      }
      if (user_access('Storm expense: view if project is assigned to team', $user)) {
        $team_member = storm_contrib_common_get_stormteam_members_from_project($project_node);
        if (!empty($team_member[$user->stormperson_nid])) {
          $access_check = FALSE;
        }
      }
      if($access_check && user_access('Storm expense: view if assigned to project', $user)) {
        if ((!empty($project_node->assigned_nid) && $project_node->assigned_nid == $user->stormperson_nid) ||
            (!empty($project_node->manager_nid) && $project_node->manager_nid == $user->stormperson_nid) ) {
          $access_check = FALSE;
        }
      }
    }

    if ($access_check) {
      $sql = stormexpense_access_sql($sql); // ACCESS PERMISSIONS BY STORMEXPENSE MODULE
    }
    $result = db_query(db_rewrite_sql($sql, 'sex', 'nid', $args), $args);
    if (!empty($result)) {
      while ($row = db_fetch_array($result)) {
        $expenses[$row['nid']] = $row;
      }
    }

  }

  return $expenses;
}


/**
 * Implementation of hook_clone_node_alter() from the node_clone module
 *
 * @param unknown_type $node
 * @param unknown_type $original_node
 * @param unknown_type $op
 * @return unknown_type
 */
function stormexpense_extension_clone_node_alter($node, $original_node, $op) {

  if (!empty($node) && !empty($original_node)) {

    switch ($op) {

      case 'prepopulate':

        storm_contrib_common_set_active_trail('storm/'. str_replace('storm', '', $original_node->type) .'s');

        $node->organization_nid = $original_node->organization_nid;
        $node->project_nid = $original_node->project_nid;
        $node->task_nid = $original_node->task_nid;
        $node->ticket_nid = $original_node->ticket_nid;
        $node->provider_title = $original_node->provider_title;
        $node->amount = $original_node->amount;
        $node->tax1app = $original_node->tax1app;
        $node->tax1percent = $original_node->tax1percent;
        $node->tax2app = $original_node->tax2app;
        $node->tax2percent = $original_node->tax2percent;
        $node->tax2percent = $original_node->tax2percent;

        break;

    }

  }
}

function stormexpense_extension_perm() {
  return array('Storm expense: view if assigned to project', 'Storm expense: view if project is assigned to team');
}

/**
 * implements hook_storm_contrib_node_access
 * add additional access to storminvoice nodes
 *
 * @param  $op
 * @param  $node
 * @param  $account
 * @param  $node_access
 * @return bool
 */
function stormexpense_extension_storm_contrib_node_access($op, $node, $account, $node_access) {
  // we only want to allow more access to some nodes, so when we have already access, we do not need to do something
  if($node_access) {
    return $node_access;
  }

  if (module_exists('stormteam') && 'view'== $op && !empty($account) && !empty($node->project_nid) && user_access('Storm expense: view if project is assigned to team', $account)) {
    // get all team members from the project
    $team_member = storm_contrib_common_get_stormteam_members_from_project($node->project_nid);

    if (!empty($team_member[$account->stormperson_nid])) {
      return TRUE;
    }
  }

  if ($op == 'view' && !empty($account) && !empty($node->project_nid) && user_access('Storm expense: view if assigned to project', $account)) {
    $node_project = node_load($node->project_nid);
    //Project assigned to person
    if (!empty($node_project->assigned_nid) && !empty($account->stormperson_nid) && $node_project->assigned_nid == $account->stormperson_nid ) {
      return TRUE;
    }
    //Person is Project Manager
    if (!empty($node_project->manager_nid) && !empty($account->stormperson_nid) && $node_project->manager_nid == $account->stormperson_nid ) {
      return TRUE;
    }
  }

  return $node_access;
}