<?php

/**
 * @file stormperson_utilisation.theme.inc
 *
 * theme functions for the module stormperson_utilisation
 */


/**
 * diplay the utilisation of a person on a day
 */
function theme_stormperson_utilisation_display_utilisation_of_day($utilisation_result, $date = 0, $display_small = FALSE, $display_department_utilisation = FALSE) {

  drupal_add_css(drupal_get_path('module', 'stormperson_utilisation') .'/stormperson_utilisation.css');

  $content  = '';

  if (empty($date)) {
    $node = new stdClass();
    $node->nid = 0;
    $date = storm_contrib_common_get_date($node);
  }

  $max_utilisation_in_hours = 0;
  $utilisation_sum_in_hours = 0;
  $percentage = 0;
  $assignments_of_day = array();

  if (is_array($utilisation_result)) {
    $max_utilisation_in_hours = $utilisation_result['max_utilisation_in_hours'];
    $utilisation_sum_in_hours = $utilisation_result['utilisation_sum_in_hours'];
    $percentage = $utilisation_result['percentage'];
    $assignments_of_day = $utilisation_result['tickets_of_day'];
  }

  // SELECTED DATE

  $content .= '<div class="stormperson-utilisation">';

  if (!$display_department_utilisation) {
    $content .= '<div class="stormbody stormperson-utilisation-date">';
    $content .= '<strong>';
    $content .= t('Utilisation on %date', array('%date' => $date));
    $content .= '</strong>';
    $content .= '</div>';
  }

  $hide_chart = FALSE;

    if ('weekend' == $utilisation_result) {
      $content .= '<strong>'. t('This is a weekend day') .'</strong>';
      $hide_chart = TRUE;
    }
    elseif ('national_holiday' == $utilisation_result) {
      $content .= '<strong>'. t('This is a national holiday day') .'</strong>';
      $hide_chart = TRUE;
    }

    // HALF DAY HOLIDAY
    if ('half_day_holiday' == $utilisation_result['holiday']) {
      $content .= '<div class="stormbody stormperson-utilisation-has-holiday" style="color:red;">';
      $content .= t('Person has a half day holiday on %value', array('%value' => $date));
      $content .= '</div>';
    }
    elseif ('national_half_holiday' == $utilisation_result['holiday']) {
      $content .= '<div class="stormbody stormperson-utilisation-has-holiday" style="color:red;">';
      $content .= t('This day is a half national holiday.');
      $content .= '</div>';
    }


    // PERSON DOES NOT WORK ON THIS WEEK DAY
    if (is_array($utilisation_result) && !empty($utilisation_result['person_does_not_work_on_weekday'])) {
      $content .= '<div class="stormbody stormperson-utilisation-person-does-not-work-on-week-day" style="color:red;">';
      $content .= t('Person normally does not work on %values', array('%value' => date('l', storm_contrib_common_date_to_time($date)))); // TODO TRANSLATION OF WEEK DAY
      $content .= '</div>';
      $hide_chart = TRUE;
    }

  if (!$display_small) {
    // MAX UTILISATION IN HOURS OF PERSON
    $content .= '<div class="stormbody stormperson-utilisation-utilisation-max">';
    $content .= theme('storm_view_item', t('Max. utilisation'), theme('storm_contrib_common_number_format', $max_utilisation_in_hours) .' '. t('hours'));
    $content .= '</div>';

    // CURRENT UTILISATION OF PERSON ON DATE IN HOURS
    $content .= '<div class="stormbody stormperson-utilisation-utilisation-current">';
    $content .= theme('storm_view_item', t('Total utilisation'), theme('storm_contrib_common_number_format', $utilisation_sum_in_hours) .' '. t('hours'));
    $content .= '</div>';

    // CURRENT PERCENTAGE OF UTILISATION
    $content .= '<div class="stormbody stormperson-utilisation-utilisation-percentage">';
    $content .= theme('storm_view_item', t('Percentage'), theme('storm_contrib_common_number_format', $percentage) .'%');
    $content .= '</div>';
  }



  // -------
  // CHART
  // -------

  $chart = '';

  // PIE 3D
  if ((100 - $percentage) < 0) {
    $values = array(
      '#legend' => t('Utilisation'),
      array('#value' => $percentage, '#label' => theme('storm_contrib_common_number_format', $percentage) .'% '. t('busy')),
    );
  }
  else {
    $values = array(
      '#legend' => t('Utilisation'),
      array('#value' => $percentage, '#label' => theme('storm_contrib_common_number_format', $percentage) .'% '. t('busy')),
      array('#value' => (100 - $percentage), '#label' => theme('storm_contrib_common_number_format', (100 - $percentage)) .'% '. t('not utilised'), '#color' => 'cccccc'),
    );
  }

  if ($display_department_utilisation) {
    if (!$hide_chart) {
      $chart = theme('storm_contrib_common_progress_bar', $percentage/100, 1);
    }
  }
  elseif ($display_small) {
    $chart = storm_contrib_common_build_chart('pie3D', $values, 270, 75);
  }
  else {
    $chart = storm_contrib_common_build_chart('pie3D', $values, 360, 100);
  }

  if (!empty($chart)) {
    $content .= '<div class="stormperson-utilisation-chart">';
    $content .= $chart;
    $content .= '</div>';
  }


  // -----------------
  // ASSIGNED TICKETS
  // -----------------
  if (!$display_department_utilisation) {
    $content .= theme('stormperson_utilisation_display_utilisation_assignments_of_day', $assignments_of_day, $date, $display_small, $display_department_utilisation);
  }

  $content .= '</div>'; // END <div class="stormbody stormperson-utilisation">

  return $content;
}

function theme_stormperson_utilisation_display_utilisation_assignments_of_day($assignments_of_day, $date = 0, $display_small = FALSE, $display_department_utilisation = FALSE) {
  $content = "";

  if (empty($assignments_of_day) && !$display_department_utilisation) {
    return $content;
  }
  $class = '';
  if (!$display_small) {
    $class = 'stormbody ';
    $content .= '<p>&nbsp;</p>';
  }
  $content .= '<div class="'. $class .'stormperson-utilisation-assigned-tickets">';
  $content .= '<label>';
  $content .= '<strong>';
  $content .= t('Assignments');
  $content .= '</strong>';
  $content .= '</label>';
  if (!empty($assignments_of_day)) {
    $padding = 5;
  }
  else {
    $padding = 20;
  }

  if ($display_small) {
    $content .= '<ul class="storm-tickets-connected Sortables" style="padding-top:'. $padding .'px; padding-bottom:'. $padding .'px; background-color: #ffffff;">';
  }
  else {
    $content .= '<ul class="storm-tickets-connected Sortables" style="padding-left:0px; padding-top:'. $padding .'px; padding-bottom:'. $padding .'px; background-color: #ffffff;">';
  }

  foreach ($assignments_of_day as $project_nid => $project_array) {

    $project_link = l($project_array['title'], 'node/'. $project_nid) .' '. t('(Project)');

    if (!empty($project_array['assignments'])) {
      foreach ($project_array['assignments'] as $assignment_nid => $assignment_node) {

        if (!empty($assignment_node)) {

          switch ($assignment_node->type) {

            case 'stormticket':
              $type = "Ticket";
              $status = $assignment_node->ticketstatus;
              break;

            case 'stormtask':
              $type = "Task";
              $status = $assignment_node->taskstatus;
              break;

          }

          $content .= '<li id="ticket-'. $assignment_node->nid .'" class="stormticket-assignment-assign-tickets-ticket" style="position: relative;">';

          $recurring_duration = '';
          if ($assignment_node->is_recurring_duration) {
            $recurring_duration = ' - '. t('recurring duration');
          }
          //ticket link
          $content .= '<div class="storm-visible">'. l($assignment_node->title, 'node/'. $assignment_node->nid);

          //popup(css) begin
          $content .= '<div class="storm-utilisation-popup">';

          $content .= t($type) .' - '. format_plural(theme('storm_contrib_common_number_format', $assignment_node->duration), '@count @unit', '@count @units', array('@unit' => $assignment_node->durationunit)) . $recurring_duration;

          $content .= ' '. storm_icon('status_'. $status, storm_attribute_value($type.' status', $status));

          if (!empty($assignment_node->datebegin) || !empty($assignment_node->dateend)) {
            $content .= '<br/>';
            if (!empty($assignment_node->datebegin)) {
              if (empty($assignment_node->dateend)) {
                $content .= t('Startdate: ');
              }
              $content .= storm_contrib_common_time_to_date(storm_contrib_common_timestamp_to_timezone($assignment_node->datebegin), storm_contrib_common_get_custom_display_date_format());
            }
            else {
              $content .= t('Enddate: ');
            }
            if (!empty($assignment_node->datebegin) && !empty($assignment_node->dateend)) {
              $content .= " - ";
            }
            if(!empty($assignment_node->dateend)) {
              $content .= storm_contrib_common_time_to_date(storm_contrib_common_timestamp_to_timezone($assignment_node->dateend), storm_contrib_common_get_custom_display_date_format());
            }
          }

          if (!empty($assignment_node->assigned_title)) {
            $content .= '<br/>'.t('Assigned to !person', array('!person' => $assignment_node->assigned_title));
          }

          $content .= '<br/>'.$project_link;

          if (!empty($assignment_node->task_nid)) {
            $content .= '<br/>'.l($assignment_node->task_title, 'node/'. $assignment_node->task_nid) .' '. t('(Task)');
          }

          //TODO: maybe add/alter popup content through drupal?

          //popup end
          $content .= '</div>';

          $content .= '</div>';

          $content .= '</li>';
        }
      }
    }

  }

  $content .= '</ul>';

  $content .= '</div>';


  return $content;
}


/**
 * theme a stormticket (utilisation layout)
 *
 * @param $person
 * @return unknown_type
 */
function theme_stormperson_utilisation_ticket_utilisation($node_ticket) {

  $content = '';
  $content .= '<li id="ticket-'. $node_ticket->nid .'" class="stormticket-assignment-assign-tickets-ticket" style="position: relative;">';
  $content .= '<span class="storm-visible">'. l($node_ticket->title, 'node/'. $node_ticket->nid) .' ('. t('Ticket') .')';
  $content .= ' '. storm_icon('status_'. $node_ticket->ticketstatus, storm_attribute_value('Ticket status', $node_ticket->ticketstatus));
  $content .= '</li>';

  return $content;
}

/**
 * theme a stormticket (utilisation layout)
 *
 * @param $person
 * @return unknown_type
 */
function theme_stormperson_utilisation_task_utilisation($node_task) {

  $content = '';
  $content .= '<li id="ticket-'. $node_task->nid .'" class="stormticket-assignment-assign-tickets-ticket" style="position: relative;">';
  $content .= '<span class="storm-visible">'. l($node_task->title, 'node/'. $node_task->nid) .' ('. t('Task') .')';
  $content .= ' '. storm_icon('status_'. $node_task->taskstatus, storm_attribute_value('Task status', $node_task->taskstatus));
  $content .= '</li>';

  return $content;
}


/**

/**
 * display the utilisation of the persons within a department
 */
function theme_stormperson_utilisation_department_utilisation($node_person, $department_array, $week) {

  drupal_add_css(drupal_get_path('module', 'stormperson_utilisation') .'/stormperson_utilisation.css');

  $content = '';

  if (!empty($node_person)) {

    if (!empty($department_array)) {

      if (!empty($week)) {

        $today = storm_contrib_common_time_to_date(time(), storm_contrib_common_get_custom_display_date_format());

        foreach ($department_array as $department_tid => $department_term_array) {

          $content .= "<br />\n";
          $content .= '<h2>'. t('Utilisation of department %name', array('%name' => $department_term_array['term']->name)) .'</h2>';
          $content .= theme('stormperson_utilisation_department_utilisation_switch', $node_person);

          // DISPLAY THE PERSONS
          if (!empty($department_term_array['persons'])) {

            $content .= '<div class="stormperson-utilisation-department-utilisation-persons">';

            $content .= '<table class="stormperson-utilisation-department-utilisation-persons-table">';

            $colspan = count($week)+1;

            // unassigned tickets and task
            $tickets = $department_term_array['unassigned'];

            // SPACER
            $content .= '<tr>';
            $content .= '<td colspan="'.$colspan.'" class="stormperson-utilisation-department-utilisation-persons-table-spacer">';
            $content .= '</td>';
            $content .= '</tr>';

            $content .= '<tr id="stormperson-utilisation-department-utilisation-person-date-row-unassigned_'.$department_tid.'">';
            // PERSON NAME
            $content .= '<td class="stormperson-utilisation-department-utilisation-persons-table-person" rowspan="2">';
            $content .= '<h3>'. t('unassigned Tickets') .'</h3>';
            $content .= '</td>';

            foreach ($week as $date) {

              $date_time = storm_contrib_common_date_to_time($date);

              $class = 'stormperson-utilisation-department-utilisation-date';

              // IF TODAY
              if ($today == $date) {
                $class = 'current-stormperson-utilisation-department-utilisation-date';
              }

              $content .= '<td class="stormperson-utilisation-department-utilisation-persons-table-td">';

              $js_ticket_switcher_class = 'utilisation-person-'.$date_time.'-unassigned_'.$department_tid.' stormperson-utilisation-department-utilisation-person-ticket-switcher';
              $content .= '<div class="'. $class .' '.$js_ticket_switcher_class.'">';
              $content .= '<strong>';
              $content .= $date;
              $content .= '</strong>';
              $content .= '</div>';

              $count_tickets = 0; $count_tasks = 0;
              foreach ($tickets[$date] as $project_array) {
                foreach ($project_array['assignments'] as $ticket_task_node) {
                  if ($ticket_task_node->type == 'stormticket') {
                    $count_tickets++;
                  }
                  else {
                    $count_tasks++;
                  }
                }
              }
              $content .= '<div class="'.$js_ticket_switcher_class.'">';
              $content .=  t("unassigned Tickets:").' <span class="utilisation-tickets-number-unassigned_'.$department_tid.'">'.$count_tickets.'</span><br/>';
              $content .=  t("unassigned Tasks:").' <span class="utilisation-tasks-number-unassigned_'.$department_tid.'">'.$count_tasks.'</span>';
              $content .= '</div>';

              $content .= '</td>';

            }

            $content .= '</tr>';
            $content .= '<tr id="stormperson-utilisation-department-utilisation-person-assignment-row-unassigned_'.$department_tid.'">';

            foreach ($week as $date) {
              $date_time = storm_contrib_common_date_to_time($date);

              $class = 'stormperson-utilisation-department-utilisation-assignment';

              // IF TODAY
              if ($today == $date) {
                $class = 'current-stormperson-utilisation-department-utilisation-assignment';
              }

              $content .= '<td id="tickets-utilisation-person-'.$date_time.'-unassigned_'.$department_tid.'" class="stormperson-utilisation-department-utilisation-persons-ticket-table-td" style="display: none" colspan="'.($colspan-1).'">';

              $content .= '<div class="'. $class .'">';

              // DISPLAY UTILISATION OF PERSON
              $content .=  theme('stormperson_utilisation_display_utilisation_assignments_of_day', $tickets[$date], $date, TRUE, TRUE);

              $content .= '</td>';
            }

            $content .= '</tr>';

            foreach ($department_term_array['persons'] as $nid_person => $node_person) {

              // SPACER
              $content .= '<tr>';
              $content .= '<td colspan="'.$colspan.'" class="stormperson-utilisation-department-utilisation-persons-table-spacer">';
              $content .= '</td>';
              $content .= '</tr>';

              $content .= '<tr id="stormperson-utilisation-department-utilisation-person-date-row-'.$node_person->nid.'">';
              // PERSON NAME
              $content .= '<td class="stormperson-utilisation-department-utilisation-persons-table-person" rowspan="2">';
              if (!empty($node_person->online_icon)) {
                $content .= '<span class="online-icon">'.$node_person->online_icon.'</span>';
              }
              if (!empty($node_person->fullname)) {
                $content .= '<h3>'. l($node_person->fullname, 'node/'.$node_person->nid) .'</h3>';
              }
              else {
                $content .= '<h3>'. l($node_person->title, 'node/'.$node_person->nid) .'</h3>';
              }
              $content .= '<span class="holidays-available">'.t("Holidays available: !holidays_available", array("!holidays_available" => $node_person->stormperson_holidays_number_holidays_available)).'</span>';
              $content .= '</td>';

              foreach ($week as $date) {

                $date_time = storm_contrib_common_date_to_time($date);

                $class = 'stormperson-utilisation-department-utilisation-date';

                // IF TODAY
                if ($today == $date) {
                  $class = 'current-stormperson-utilisation-department-utilisation-date';
                }

                $add_class = stormperson_utilisation_theme_helper_utilisation_class($node_person->utilisation_calculation[$date]);

                $content .= '<td class="stormperson-utilisation-department-utilisation-persons-table-td '. $add_class .'">';

                $js_ticket_switcher_class = 'utilisation-person-'.$date_time.'-'.$node_person->nid.' stormperson-utilisation-department-utilisation-person-ticket-switcher';
                $content .= '<div class="'. $class .' '.$js_ticket_switcher_class.'">';
                $content .= '<strong>';
                $content .= $date;
                $content .= '</strong>';
                $content .= '</div>';

                // DISPLAY UTILISATION OF PERSON
                $content .= '<div class="'.$js_ticket_switcher_class.'">';
                $content .=  theme('stormperson_utilisation_display_utilisation_of_day', $node_person->utilisation_calculation[$date], $date, TRUE, TRUE);
                $content .= '</div>';

                if (module_exists('stormperson_holidays') && !empty($node_person->stormperson_holidays_holidays[storm_contrib_common_timestamp_to_day_gm_time($date_time)]) && $node_person->stormperson_holidays_holidays[storm_contrib_common_timestamp_to_day_gm_time($date_time)]['permitted'] == 0
                    && stormperson_holidays_person_holidays_permit_access($node_person)) {
                  $content .= '<div class="stormperson-utilisation-department-utilisation-holiday-request">'.l(t('Requested Holiday'), 'node/'.$node_person->nid.'/holidays' ).'</div>';
                }

                $content .= '</td>';

              }

              $content .= '</tr>';
              $content .= '<tr id="stormperson-utilisation-department-utilisation-person-assignment-row-'.$node_person->nid.'">';

              foreach ($week as $date) {
                $date_time = storm_contrib_common_date_to_time($date);

                $class = 'stormperson-utilisation-department-utilisation-assignment';

                // IF TODAY
                if ($today == $date) {
                  $class = 'current-stormperson-utilisation-department-utilisation-assignment';
                }

                $add_class = stormperson_utilisation_theme_helper_utilisation_class($node_person->utilisation_calculation[$date]);

                $content .= '<td id="tickets-utilisation-person-'.$date_time.'-'.$node_person->nid.'" class="stormperson-utilisation-department-utilisation-persons-ticket-table-td '. $add_class .'" style="display: none" colspan="'.($colspan-1).'">';

                $content .= '<div class="'. $class .'">';

                // DISPLAY UTILISATION OF PERSON
                $tickets = (is_array($node_person->utilisation_calculation[$date]) && !empty($node_person->utilisation_calculation[$date]['tickets_of_day'])) ? $node_person->utilisation_calculation[$date]['tickets_of_day'] : array();
                $content .=  theme('stormperson_utilisation_display_utilisation_assignments_of_day', $tickets, $date, TRUE, TRUE);

                $content .= '</td>';
              }

              $content .= '</tr>';

            }

            $content .= '</table>';

            $content .= '</div>';

          }

        }

      }
      else {
        $content .= t('No dates available');
      }

    }
    else {
      $content .= t('No department availabale');
    }

  }
  else {
    $content .= t('No person availabale');
  }

  return $content;
}


/**
 * theme the switch of the department utilisation to switch between day and week
 */
function theme_stormperson_utilisation_department_utilisation_switch($node_person = NULL) {

  $request_uri = request_uri();

  $content = '';

  if (!empty($node_person)) {
    $date_range = storm_contrib_common_get_date_range($node_person);
    $query_date = 'date='.storm_contrib_common_time_to_date($date_range['selected_date_from'], 'Y-m-d');
  }


  if (strpos($request_uri, '/storm/department-utilisation-by-day') !== FALSE ) {
    $content .= t('Utilisation by day');
  }
  else {
    $content .= l(t('Utilisation by day'), 'storm/department-utilisation-by-day', array('query' => $query_date));
  }

  $content .= ' | ';

  if (strpos($request_uri, 'storm/department-utilisation-by-week') !== FALSE ) {
    $content .= t('Utilisation by week');
  }
  else {
    $content .= l(t('Utilisation by week'), 'storm/department-utilisation-by-week', array('query' => $query_date));
  }

  $content .= ' | ';

  if (strpos($request_uri, 'storm/department-utilisation-by-month') !== FALSE ) {
    $content .= t('Utilisation by month');
  }
  else {
    $content .= l(t('Utilisation by month'), 'storm/department-utilisation-by-month', array('query' => $query_date));
  }


  return $content;
}

function theme_stormperson_utilisation_person_utilisation($data, $display_small) {
  global $user;
  $content = '';

  drupal_add_css(drupal_get_path('module', 'stormperson_utilisation') .'/stormperson_utilisation.css');

  $today = storm_contrib_common_time_to_date(time(), storm_contrib_common_get_custom_display_date_format());
  $max_utilisation_percentage = stormperson_utilisation_get_max_utilisation_percentage_per_day();

  $content .= '<table class="utilisation-list-days">';
  foreach ($data['date']['days'] as $date) {
    $tr_class = stormperson_utilisation_theme_helper_utilisation_class($data['utilisation'][$date]);
    if ($today == $date) {
      $tr_class .= " today";
    }
    $content .= '<tr class="'.$tr_class.'">';
    $content .= '<td class="utilisation-list-item-date">'.$date.'</td>';
    $content .= '<td class="utilisation-list-item-utilisation">';

    $hide_assignments = FALSE;

    if ('weekend' == $data['utilisation'][$date]) {
      $content .= '<strong>'. t('This is a weekend day') .'</strong>';
      $hide_assignments = TRUE;
    }
    elseif ('national_holiday' == $data['utilisation'][$date]) {
      $content .= '<strong>'. t('This is a national holiday day') .'</strong>';
      $hide_assignments = TRUE;
    }

    // HALF DAY HOLIDAY
    if ('half_day_holiday' == $data['utilisation'][$date]) {
      $content .= '<div class="stormbody stormperson-utilisation-has-holiday" style="color:red;">';
      $content .= t('Person has a half day holiday on %value', array('%value' => $date));
      $content .= '</div>';
    }
    elseif ('national_half_holiday' == $data['utilisation'][$date]) {
      $content .= '<div class="stormbody stormperson-utilisation-has-holiday" style="color:red;">';
      $content .= t('This day is a half national holiday.');
      $content .= '</div>';
    }

    // PERSON DOES NOT WORK ON THIS WEEK DAY
    if (is_array($data['utilisation'][$date]) && !empty($data['utilisation'][$date]['person_does_not_work_on_weekday'])) {
      $content .= '<div class="stormbody stormperson-utilisation-person-does-not-work-on-week-day" style="color:red;">';
      $content .= t('Person normally does not work on %values', array('%value' => date('l', storm_contrib_common_date_to_time($date)))); // TODO TRANSLATION OF WEEK DAY
      $content .= '</div>';
      $hide_assignments = TRUE;
    }

    if (!$hide_assignments) {
      $add_class = "";
      if ($data['utilisation'][$date]['percentage'] > $max_utilisation_percentage) {
        $add_class = "utilisation-overloaded";
      }
      $content .= '<div class="stormbody stormperson-utilisation-utilisation-percentage '.$add_class.'">';
      $content .= t('Utilisation').": ". theme('storm_contrib_common_number_format', $data['utilisation'][$date]['percentage']) .'%';
      $content .= '</div>';

      if (is_array($data['trackings']) && !empty($data['trackings'][$date])) {
        $content .= '<div class="stormbody stormperson-utilisation-timetracking">';
        $tracked_time = t('!trackingh of !maxh', array('!tracking' =>theme('storm_contrib_common_number_format', $data['trackings'][$date]['duration']), '!max' => theme('storm_contrib_common_number_format', $data['utilisation'][$date]['max_utilisation_in_hours'])));
        if ($user->stormperson_nid == $data['person']->nid && user_access('Storm timetracking: add')) {
          $tracked_time .= $data['trackings'][$date]['add_icon'];
        }
        $content .= t('Timetracking') .": ". $tracked_time;
        $content .= '</div>';
      }

      $tickets  = (is_array($data['utilisation'][$date]) && !empty($data['utilisation'][$date]['tickets_of_day'])) ? $data['utilisation'][$date]['tickets_of_day'] : array();
      $content .= '<div class="utilisation-list-item-utilisation-assignments">'.theme('stormperson_utilisation_display_utilisation_assignments_of_day', $tickets, $date, $display_small, false);
    }

    $content .= '</td>';
    $content .= '</tr>';
  }
  $content .= '</table>';

  return $content;
}