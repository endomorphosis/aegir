<?php
// Id: $

/**
 * @file stormtimetracking_favorites.module
 */



/**
 * node types to use
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_node_types() {
  return array(
    'stormticket',
    'stormtask',
    'stormproject',
  );
}

/**
 * the weight of the button of each node type
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_node_type_weights() {
  return array(
    'stormticket' => -15,
    'stormtask' => -10,
    'stormproject' => 0,
  );
}


/**
 * Implementation of hook_menu()
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_menu() {
  $items = array();

  $items['storm/timetracking-favorites'] = array(
    'title' => 'Timetracking Favorites',
    'page callback' => 'stormtimetracking_favorites_list',
    'access arguments' => array('Storm timetracking: access'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}


/**
 * Implementation of hook_theme()
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_theme() {
  return array(
    'stormtimetracking_favorites_form' => array(
      'file'      => 'stormtimetracking_favorites.theme.inc',
      'arguments' => array('form' => array()),
    ),
    'stormtimetracking_favorites_list' => array(
      'file'      => 'stormtimetracking_favorites.theme.inc',
      'arguments' => array('user' => NULL),
    ),

  );
}


/**
 * form to add or remove a node to the favorites
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_form($form_state, $node, $display_as_stormlink = TRUE) {

  global $user;

  if (!empty($node)) {

    $form = array();

    $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
    );

    $form['node'] = array(
    '#type' => 'value',
    '#value' => $node,
    );

    $form['display_as_stormlink'] = array(
    '#type' => 'value',
    '#value' => $display_as_stormlink,
    );

    if (stormtimetracking_favorites_check_if_nid_is_favorite($node->nid, $user)) {
      $value = t('Remove from timetracking favorites');
    }
    else {
      $value = t('Add to timetracking favorites');
    }

    $form['submit_'.$node->nid] = array(
    '#type' => 'submit',
    '#value' => $value,
    );


  }

  return $form;
}


/**
 * submit of form to add or remove a node to the favorites
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_form_submit($form, &$form_state) {

  global $user;

  $request_uri = request_uri();

  if ('/storm/timetracking-favorites' == $request_uri) {

    // ON FAVORITES LIST PAGE THERE ARE MULTIPLE FORMS
    // SO DO NOT USE $form_state BUT $_POST
    $nid = (int) filter_xss($_POST['nid'], array());
    if (!is_numeric($nid)) {
      return;
    }
  }
  else {
    $node = $form_state['values']['node'];
    $nid = $form_state['values']['nid'];
  }

  if (!empty($nid) && is_numeric($nid)) {

    switch ($form_state['values']['op']) {

      // ADD TO FAVORITES
      case t('Add to timetracking favorites'):
        stormtimetracking_favorites_insert_favorite($node);
        break;

      // REMOVE FROM FAVORITES
      case t('Remove from timetracking favorites'):
        stormtimetracking_favorites_delete_favorite($user, $nid);
        break;

    }

  }

}


/**
 * insert a favorite
 *
 * @param unknown_type $node_favorite
 * @return unknown_type
 */
function stormtimetracking_favorites_insert_favorite($node, $account = NULL) {
  global $user;

  if (empty($account)) {
    $account = $user;
  }

  if (!empty($node)) {

    switch ($node->type) {

      case 'stormproject':
        $project_nid = $node->nid;
        $project_title = $node->title;
        break;

      default:
        $project_nid = $node->project_nid;
        $project_title = $node->project_title;
        break;

    }

      $object = new stdClass();
      $object->nid      = $node->nid;
      $object->uid      = $account->uid;
      $object->organization_nid = $node->organization_nid;
      $object->organization_title = $node->organization_title;
      $object->project_nid = $project_nid;
      $object->project_title = $project_title;
      $object->task_nid = $node->task_nid;
      $object->task_title = $node->task_title;
      $object->datebegin = $node->datebegin;
      $object->dateend = $node->dateend;
      $object->durationunit = $node->durationunit;
      $object->duration = $node->duration;

      drupal_write_record('stormtimetracking_favorites', $object, array());

  }

}

/**
 * delete favorite(s)
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_delete_favorite($user = NULL, $nid = 0) {

  if (!empty($user) || !empty($nid)) {

    $args = array();
    $where = array();
    $sql  = 'DELETE FROM {stormtimetracking_favorites} WHERE ';
    if (!empty($user->uid)) {
      $where[] = 'uid = %d ';
      $args[] = $user->uid;
    }

    if (!empty($nid)) {
      $where[] = ' nid = %d ';
      $args[] = $nid;
    }

    $sql .= implode(' AND ', $where);

    db_query($sql, $args);

  }

}


/**
 * load the favorites
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_load_favorites($user, $filter=array()) {

  if (empty($filter['order'])) {
    $filter['order'] = "ORDER BY stf.organization_title ASC, stf.project_title ASC, n.title ASC";
  }

  $args = array();
  $sql  = 'SELECT n.title, n.type, n.uid, stf.nid, stf.organization_nid, stf.organization_title, stf.project_nid, stf.project_title, ';
  $sql .= 'stf.task_nid, stf.task_title, stf.datebegin, stf.dateend, stf.durationunit, stf.duration, n.vid ';
  $sql .= 'FROM {stormtimetracking_favorites} stf ';
  $sql .= 'INNER JOIN {node} n ON n.nid = stf.nid ';
  $sql .= 'WHERE stf.uid = %d ';
  $args[] = $user->uid;
  if (!empty($filter['where'])) {
    $sql .= 'AND '.implode(' AND ', $filter['where']).' ';
    if (!empty($filter['where_args'])) {
      $args = array_merge($args, $filter['where_args']);
    }
  }
  $sql .= $filter['order'];


  $result = db_query(db_rewrite_sql($sql, 'stf', 'nid', $args), $args) ;
  if (!empty($result)) {
    while ($row = db_fetch_array($result)) {

      if (!empty($row['nid'])) {

        $user->stormtimetracking_favorites[$row['nid']] = array();
        $user->stormtimetracking_favorites[$row['nid']]['nid'] = $row['nid'];
        $user->stormtimetracking_favorites[$row['nid']]['type'] = $row['type'];
        $user->stormtimetracking_favorites[$row['nid']]['title'] = $row['title'];
        $user->stormtimetracking_favorites[$row['nid']]['uid'] = $row['uid'];
        $user->stormtimetracking_favorites[$row['nid']]['organization_nid'] = $row['organization_nid'];
        $user->stormtimetracking_favorites[$row['nid']]['organization_title'] = $row['organization_title'];
        $user->stormtimetracking_favorites[$row['nid']]['project_nid'] = $row['project_nid'];
        $user->stormtimetracking_favorites[$row['nid']]['project_title'] = $row['project_title'];
        $user->stormtimetracking_favorites[$row['nid']]['task_nid'] = $row['task_nid'];
        $user->stormtimetracking_favorites[$row['nid']]['task_title'] = $row['task_title'];
        $user->stormtimetracking_favorites[$row['nid']]['datebegin'] = $row['datebegin'];
        $user->stormtimetracking_favorites[$row['nid']]['dateend'] = $row['dateend'];
        $user->stormtimetracking_favorites[$row['nid']]['duration'] = $row['duration'];
        $user->stormtimetracking_favorites[$row['nid']]['durationunit'] = $row['durationunit'];

      }

    }
  }

}


/**
 * load the favorites
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_check_if_nid_is_favorite($nid, $account) {

  if (!empty($nid) && is_numeric($nid) && !empty($account->uid) && is_numeric($account->uid)) {

    $args = array();
    $sql  = 'SELECT n.uid ';
    $sql .= 'FROM {stormtimetracking_favorites} stf ';
    $sql .= 'INNER JOIN {node} n ON n.nid = stf.nid ';
    $sql .= 'WHERE stf.nid = %d ';
    $args[] = $nid;
    $sql .= 'AND stf.uid = %d ';
    $args[] = $account->uid;
    $result = db_result(db_query(db_rewrite_sql($sql, 'stf', 'nid', $args), $args)) ;
    if (!empty($result)) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Implementation of hook_nodeapi()
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

  $node_types = stormtimetracking_favorites_node_types();

  switch ($op) {

    case 'update':

      // UPDATE ENTRIES IN {stormtimetracking_favorites} table

      switch ($node->type) {

        case 'stormorganization':
          db_query("UPDATE {stormtimetracking_favorites} SET organization_title = '%s' WHERE organization_nid = %d ", $node->title, $node->nid);
          break;

        case 'stormproject':
          db_query("UPDATE {stormtimetracking_favorites} SET project_title = '%s' WHERE project_nid = %d ", $node->title, $node->nid);
          break;

        case 'stormtask':
          db_query("UPDATE {stormtimetracking_favorites} SET task_title = '%s' WHERE task_nid = %d ", $node->title, $node->nid);
          break;

      }
      if (in_array($node->type, $node_types)) {
        db_query("UPDATE {stormtimetracking_favorites} SET datebegin = %d, dateend = %d, durationunit = '%s', duration = %d WHERE nid = %d ",
        $node->datebegin, $node->dateend, $node->durationunit, $node->duration, $node->nid);
      }


      break;

    case 'delete':
      if (in_array($node->type, $node_types)) {
        stormtimetracking_favorites_delete_favorite(NULL, $node->nid);
      }
      break;

    case 'view':
      if (in_array($node->type, $node_types)) {

        $weights = stormtimetracking_favorites_node_type_weights();

        $node->content['stormtimetracking_favorites'] = array(
        '#value' => drupal_get_form('stormtimetracking_favorites_form', $node),
        '#weight' => $weights[$node->type],
        );
      }
      break;

  }

}


/**
 * Implementation of hook_user()
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_user($op, &$edit, &$account, $category = NULL) {

  switch ($op) {

    case 'delete':
      stormtimetracking_favorites_delete_favorite($account);
      break;

  }

}


/**
 * list all favorites of a user
 *
 * @return unknown_type
 */
function stormtimetracking_favorites_list() {

  $breadcrumb = array();
  $breadcrumb[] = l(t('Storm'), 'storm');
  drupal_set_breadcrumb($breadcrumb);

  global $user;

  $header = array(
    array('data' => t('Organization'), 'field' => 'stf.organization_title'),
    array('data' => t('Project'), 'field' => 'stf.project_title', 'sort' => 'asc'),
    array('data' => t('Title'), 'field' => 'n.title'),
    array('data' => t('Type'), 'field' => 'n.type'),
    array('data' => t('Options')),
  );

  $filter = array('where' => array(), 'where_args' => array());
  if (!empty($_GET['organization_nid']) && is_numeric($_GET['organization_nid'])) {
    $filter['where'][] = "stf.organization_nid = %d";
    $filter['where_args'][] = $_GET['organization_nid'];
  }
  if (!empty($_GET['project_nid']) && is_numeric($_GET['project_nid'])) {
    $filter['where'][] = "stf.project_nid = %d";
    $filter['where_args'][] = $_GET['project_nid'];
  }


  $filter['order'] = tablesort_sql($header).", n.title ASC";

  stormtimetracking_favorites_load_favorites($user, $filter);


  $content = theme('stormtimetracking_favorites_list', $user, $header);
  return $content;
}


/**
 * add links to the storm dashboard
 *
 * @return array $links
 */
function stormtimetracking_favorites_storm_dashboard_links($type) {
  $links = array();
  if ($type == 'page' || $type == 'block') {

    // TIMETRACKING FAVORITES
    $links[] = array(
      'theme' => 'storm_dashboard_link',
      'title' => t('Timetracking Favorites'),
      'icon' => 'stormtimetrackings',
      'path' => 'storm/timetracking-favorites',
      'params' => array(),
      'access_arguments' => 'Storm timetracking: access',
      'node_type' => 'stormtimetracking',
      'add_type' => '',
      'map' => array(),
      'weight' => 8.5,
    );
  }

  return $links;
}
