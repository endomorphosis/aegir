** New issues added should be added at the TOP of the list for each version as they are committed. **

Version 6.x-2.0-beta2:
- Task #1412598 by kfritsche: Added implementation of new storm dashboard hook.

Version 6.x-2.0-beta1:
- Task #1392484 by juliangb: Separate Storm Knowledgebase into separate project.
