<?php
/**
 * @file
 * Views Slideshow Xtra theming functions.
 */

/**
 * Views Slideshow Widget rendering.
 *
 * @ingroup themeable
 */
function theme_views_slideshow_xtra_widget_render($vss_id, $view, $settings, $location, $rows) {

  // Add javascript settings for the pager type.
  $js_vars = array(
    'viewsSlideshowXtra' => array(
      $vss_id => array(
        'displayDelay' => $settings['display_delay'],
        'displayDelayFade' => $settings['display_delay_fade'],
        'pauseAfterMouseMove' => $settings['pause_after_mouse_move'],
      ),
    ),
  );
  drupal_add_js($js_vars, 'setting');
  drupal_add_css(drupal_get_path('module', 'views_slideshow_xtra').'/views_slideshow_xtra.css');
  drupal_add_js(drupal_get_path('module', 'views_slideshow_xtra').'/views_slideshow_xtra.js');

  $output = '<div id="views-slideshow-xtra-' . $vss_id . '" class="views-slideshow-xtra-wrapper">';

  $count = 0;
  $printout = 0;

  // Render each slide.
  foreach ($view->result as $count => $node) {
    // Render each field.
    foreach ($settings['fields'] as $field) {
      $item_indexes = array('text' => 0, 'link' => 0, 'image' => 0);
      if (!empty($view->field[$field])) {
        if (!$printout) {
          $tempFieldInfo = $view->field[$field];
        }

        $output .= '<div class="views-slideshow-xtra-row views-slideshow-xtra-row-' . $count . '">';
        $fields = array();

        if (isset($view->field[$field]->field_values) && !empty($view->field[$field]->field_values[$node->nid][0]['value'])) {
          $fields = $view->field[$field]->field_values[$node->nid];
        }
        elseif (!empty($view->field[$field]->content_field) && $view->field[$field]->content_field['module'] == 'computed_field') {
          $fields[] = array('value' => $view->style_plugin->rendered_fields[$count][$field]);
        }
        elseif (!empty($view->field[$field]->original_value)) {
          $fields[] = array('value' => $view->field[$field]->original_value);
        }

        if (!empty($fields)) {
          foreach ($fields as $field_info) {
            $vsx_obj = json_decode($field_info['value']);
            $vsx = get_object_vars($vsx_obj);
            if (!is_array($vsx)) {
              $vsx = array();
            }

            $vsx += array(
              'type' => 'text',
              'left' => 0,
              'top' => 0,
              'text' => '',
              'url' => '',
              'lightbox' => false,
              'width' => 900,
              'height' => 600,
              'classes' => '',
              'styles' => '',
              'src' => '',
              'target' => '',
            );

            // Pre-append top and left values to styles
            $vsx['styles'] = 'top:' . $vsx['top'] . 'px;' . 'left:' . $vsx['left'] . 'px;' . $vsx['styles'];

            $output .= theme('views_slideshow_xtra_' . $vsx['type'], $vss_id, $view, $vsx, $count, $item_indexes[$vsx['type']]);

            $item_indexes[$vsx['type']] = $item_indexes[$vsx['type']] + 1;
          }
        }
        $output .= '</div>';
      }
    }
    $count++;
  }

  $output .= '</div>';
  return $output;
}

/**
 * Text element template_preprocess.
 */
function _views_slideshow_xtra_preprocess_text(&$vars) {
  // Classes
  $vars['attributes']['class'] = 'views-slideshow-xtra-element';
  $vars['attributes']['class'] .= ' views-slideshow-xtra-text-' . $vars['slide_count'] . '-' . $vars['field_item_count'];
  if(!empty($vars['vsx']['classes'])) {
    $vars['attributes']['class'] .= ' ' . $vars['vsx']['classes'];
  }

  // Styles variable
  $vars['attributes']['style'] = $vars['vsx']['styles'];

  // Text variable
  $vars['text'] = $vars['vsx']['text'];

  // Send data to Javascript
  $js_vars = array(
    'viewsSlideshowXtra' => array(
      $vars['vss_id'] => array(
        'slideInfo' => array(
          'text' => array(
            'slide-' . $vars['slide_count'] => array(
              'item-' . $vars['field_item_count'] => array(
                'left' => $vars['vsx']['left'],
                'top' => $vars['vsx']['top'],
              ),
            ),
          ),
        ),
      ),
    ),
  );
  drupal_add_js($js_vars, 'setting');
}

/**
 * Link element template_preprocess.
 */
function _views_slideshow_xtra_preprocess_link(&$vars) {

  // Classes
  $vars['attributes']['class'] = 'views-slideshow-xtra-element';
  $vars['attributes']['class'] .= ' views-slideshow-xtra-link-' . $vars['slide_count'] . '-' . $vars['field_item_count'];
  if(!empty($vars['vsx']['classes'])) {
    $vars['attributes']['class'] .= ' ' . $vars['vsx']['classes'];
  }

  // Styles variable
  $vars['attributes']['style'] = $vars['vsx']['styles'];

  // Text variable
  $vars['text'] = $vars['vsx']['text'];

  // URL variable
  $vars['url'] = $vars['vsx']['url'];

  $js_vars = array(
    'viewsSlideshowXtra' => array(
      $vars['vss_id'] => array(
        'slideInfo' => array(
          'link' => array(
            'slide-' . $vars['slide_count'] => array(
              'item-' . $vars['field_item_count'] => array(
                'left' => $vars['vsx']['left'],
                'top' => $vars['vsx']['top'],
              ),
            ),
          ),
        ),
      ),
    ),
  );
  drupal_add_js($js_vars, 'setting');

  // Lightbox
  if ($vars['vsx']['lightbox']) {
    $vars['url'] .= '?width=' . $vars['vsx']['width'] . '&height=' . $vars['vsx']['height'] . '&iframe=true';
  }

}

/**
 * Image element template_preprocess.
 */
function _views_slideshow_xtra_preprocess_image(&$vars) {

  // Classes
  $vars['attributes']['class'] = 'views-slideshow-xtra-element';
  $vars['attributes']['class'] .= ' views-slideshow-xtra-image-' . $vars['slide_count'] . '-' . $vars['field_item_count'];
  if(!empty($vars['vsx']['classes'])) {
    $vars['attributes']['class'] .= ' ' . $vars['vsx']['classes'];
  }

  // Styles variable
  $vars['attributes']['style'] = $vars['vsx']['styles'];

  // Text variable
  $vars['text'] = $vars['vsx']['text'];

  // url variable
  $vars['url'] = $vars['vsx']['url'];

  // src variable
  $vars['src'] = $vars['vsx']['src'];

  // target variable
  $vars['target'] = $vars['vsx']['target'];

  $js_vars = array(
    'viewsSlideshowXtra' => array(
      $vars['vss_id'] => array(
        'slideInfo' => array(
          'image' => array(
            'slide-' . $vars['slide_count'] => array(
              'item-' . $vars['field_item_count'] => array(
                'left' => $vars['vsx']['left'],
                'top' => $vars['vsx']['top'],
              ),
            ),
          ),
        ),
      ),
    ),
  );
  drupal_add_js($js_vars, 'setting');

}
